# DIY MaKey MaKey by micro:bit

Are you enthusiastic about MaKey MaKey?! I am. With this little piece of hardware you can connect your computer to everything in the world that is conductive. Playing a piano by a banana? Flappy Bird flying by you jumping in a box of water? It's all possible. But unfortunately nowadays it comes at a higher price than the cheapest Android smartphones. Also with the newest board revisions it's not that open anymore. So let's have a micro:bit act like a MaKey MaKey connected to your computer as a keyboard for around 25 USD.

![](https://gitlab.com/jurjendevries/makey-makey-microbit/-/raw/main/makeymakey-microbit-example.jpg)

## Components needed
* [Micro:bit V2](https://microbit.org/buy/)
* Optionally a [Micro:bit Breakout board](https://kitronik.co.uk/products/5601b-edge-connector-breakout-board-for-bbc-microbit-pre-built) when you want to use more than 3 keys. If you have good soldering tools and skills you can use that as an alternative.
* 4 or 6 Wires. The original Makey MaKey comes included with alligator clips. In my setup I am using female to male jumper wires to connect from the breakout board to the fruit. It depends on your project and if you will solder on the microbit directly which cables and length you will exactly need. If you don't solder and want to be able to connect it to multiple objects I would recommend finding [female jumper wires to alligator clips](https://aliexpress.com/item/1005001699080243.html).
* Something conductive to connect to your the wires such as your body, fruit, etc. Note. Don't connect the wires to your body and a power socket 💀
* A computer / phone / tablet with bluetooth support. Please note the microbit blehid extension may not work with all bluetooth devices, check the list of [compatible devices](https://github.com/bsiever/microbit-pxt-blehid/discussions).

The total cost of the DIY MaKey MaKey will be around 28 USD, which is almost half of the price of the MaKey MaKey. Note the price indication is last updated by February 2023 and may vary by your location.

## Getting started

To make it easy for you to get started with the DIY MaKey MaKey by a micro:bit, here's a list of recommended next steps.

## Installation
To program the microbit we are using [MakeCode](https://makecode.microbit.org/). Click import and thereafter Import a URL. Copy / paste the URL https://makecode.microbit.org/_Thaha5bWh4uc
You will now see and understand the code and download it to your micro:bit. Alternatively you can [download the HEX file](https://gitlab.com/jurjendevries/makey-makey-microbit/-/raw/main/DIY-Makey-Makey-microbit.hex) directly to your micro:bit. Please note you might need to add the microbit-pxt-blehid extension in MakeBlock. This extension provides the functionality to have your micro:bit bluetooth connected as a keyboard.

## Assembling
As described in the [micro:bit pin documentation](https://makecode.microbit.org/device/pins) 'There are 5 large pins that are also connected to holes in the board labelled: 0, 1, 2, 3V, and GND. And along the same edge, there are 20 small pins that you can use when plugging the micro:bit into an edge connector.' If you only need to emulate 3 keys such as space, left and right you are good to go without the breakout board and connect them directly. If you want to use 5 keys such as space, left, right, up and down you can use the breakout board. Connect the female jumper wires into 0 (left), 5 (up / button A), 1 (right), 11 (down / button B), 2 (spacebar) and the last one in 0V.

![](https://gitlab.com/jurjendevries/makey-makey-microbit/-/raw/main/assembling-microbit-breakout.jpg)

## Usage
Power your micro:bit and connect the bluetooth to a device that can handle a bluetooth keyboard. See '2. Have your device connect to the micro:bit' at the [blehid instructions](https://makecode.microbit.org/pkg/bsiever/microbit-pxt-blehid). If the bluetooth connection is successful you will see a smile on the Microbit led. When there is no bluetooth connection it will be a sad smiley face.

Now you should be able to use the wires as a conductive keyboard for the cursor and space keys. Hold the 0V (Ground/GND) to make the circuit. You can create something around this, [play Flappybird](https://flappybird.io/) by the emulated spacebar or try one of the provided [MaKey MaKey apps](https://makeymakey.com/pages/plug-and-play-makey-makey-apps).

## Authors and acknowledgment
Jurjen de Vries
A Special thank you to [Bill Siever](https://github.com/bsiever) for providing the micro:bit blehid extension.

## License
This code and documentation is free open source licensed by [MIT license](https://gitlab.com/jurjendevries/makey-makey-microbit/-/blob/main/LICENSE).
