input.onPinPressed(TouchPin.P0, function () {
    keyboard.sendString(keyboard.keys(keyboard._Key.left))
})
bluetooth.onBluetoothConnected(function () {
    basic.showIcon(IconNames.Happy)
})
bluetooth.onBluetoothDisconnected(function () {
    basic.showIcon(IconNames.Sad)
})
input.onButtonPressed(Button.A, function () {
    keyboard.sendString(keyboard.keys(keyboard._Key.up))
})
input.onPinPressed(TouchPin.P2, function () {
    keyboard.sendString(" ")
})
input.onButtonPressed(Button.B, function () {
    keyboard.sendString(keyboard.keys(keyboard._Key.down))
})
input.onPinPressed(TouchPin.P1, function () {
    keyboard.sendString(keyboard.keys(keyboard._Key.right))
})
keyboard.startKeyboardService()
